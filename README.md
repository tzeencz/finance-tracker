# README

## Finance-Tracker

----

#### Description

(App built with "The complete RoR Developer Course" from Udemy)

Finance-tracker is an practice app meant to be an proof of concept app, built in
relatively short time, tests are not required - time is of utmost essence while
delivering core functionalities.

#### Core functionalities/ assumptions

- Authentication system, users can sign-up, edit their profile, log in/out,
- Users can track stocks, up to 10 per user, their profile page will display them,
- Users can search for stock symbols using a search bar,
- Users can choose to add a stock from results of search to their portfolio,
- Users can look for friends, or other users of the app, by first or last name or
  email,
- Users can view portfolio of stocks their friends are tracking for investing
  ideas
- App must be mobile friendly, so the styling has to be responsive.

#### Technology

- Ruby on Rails v.5.1.5

- gems:
